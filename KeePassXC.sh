#!/usr/bin/env bash

################################################################################
# Script                                                                       #
# Copyright (C) 2023-2025 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# Type: Autostart
# Reason/goal: KeePassXC does not yet have an auto login feature with user
# password (pam). This takes the database password from KWallet, and opens it
# Dependencies: KWallet, KeePassXC

kwallet-query -f Passwords -r KeePassXC kdewallet | keepassxc \
	--pw-stdin ~/Projects/Secrets/Passwords.kdbx
